[代码] [Java]代码
view source
print?
package com.system.oss.common.util;
 
	 
/**
 * 动态生成随机字符数组
 * 
 * @author 苏持恒
 */
public class ShortMessageCodeUntil
{
 
	 
  /**
 * 随机生成4位数字字符数组
 * 
 * @return rands
 */
 public static char[] generateCheckCode()
  {
 String chars = "0123456789";
 char[] rands = new char[4];
 for (int i = 0; i < 4; i++)
  {
 int rand = (int) (Math.random() * 10);
  rands[i] = chars.charAt(rand);
 }
  return rands;
   }
	 
	 
	    /**
* 随机生成6位数字字符数组
* 
* @return rands
 */
 public static char[] generateCheckPass()
 {
String chars = "0123456789";
 char[] rands = new char[6];
 for (int i = 0; i < 6; i++)
 {
 int rand = (int) (Math.random() * 10);
 rands[i] = chars.charAt(rand);
 }
 return rands;
 }
	 
	 
}